## How to use

Install dependencies using either Yarn (yarn install) or NPM (npm install).

## Local Development

In order to develop locally, run either `yarn dev`, or `npm run dev`, and it will open up in localhost:3000.

## Export

To build files and export them as static, run `yarn export` or `npm run export`
