import "../styles/main.css";
import {useEffect} from "react";
import TagManager from 'react-gtm-module';

function MyApp({ Component, pageProps }) {

    // Google Tag Manager
    useEffect(() => {
        TagManager.initialize({ gtmId: 'GTM-PQCNP9X' });
    }, []);

  return <Component {...pageProps} />;
}

export default MyApp;
