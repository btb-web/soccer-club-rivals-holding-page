import Head from "next/head";
import ScrLogo from "../components/ScrLogo";

export default function Home() {
  return (
    <>
      <Head>
        <title>Soccer Club Rivals</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col items-center justify-center w-full relative overflow-hidden min-h-screen flex-1 px-3 text-center pb-56 md:pb-44 ">
        <picture>
          <source
            media="(min-width: 641px)"
            srcSet="/images/bg.avif"
            type="image/avif"
          />
          <source
            media="(min-width: 641px)"
            srcSet="/images/bg.webp"
            type="image/webp"
          />
          <source
            media="(min-width: 641px)"
            srcSet="/images/bg.jpeg"
            type="image/jpeg"
          />

          <source srcSet="/images/bg-mobile.avif" type="image/avif" />
          <source srcSet="/images/bg-mobile.webp" type="image/webp" />
          <source srcSet="/images/bg-mobile.jpeg" type="image/jpeg" />

          <img
            className=" absolute inset-0 w-full h-screen object-cover"
            src="/images/SCR_background.jpeg"
            alt="Background"
          />
        </picture>

        <ScrLogo className="z-10 h-36 w-52" />

        <h1 className="z-10 text-[4rem] lg:text-[7.5rem] uppercase text-blue-main font-serif mt-14">
          Coming Soon
        </h1>

        <img
          class="absolute -bottom-8 -right-12 top-0 h-full w-full object-cover object-left-bottom md:left-[14rem] lg:left-[24rem] lg:object-contain lg:object-right-bottom  2xl:left-0"
          src="/images/svg/bg-crosses.svg"
        />
      </main>
    </>
  );
}
