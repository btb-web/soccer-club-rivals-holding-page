import Document, { Html, Head, Main, NextScript } from "next/document";
import Fonts from "../components/Fonts";

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <Fonts />
        </Head>
        <body className="min-h-screen">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
