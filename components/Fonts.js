const Fonts = () => {
  return (
    <>
      <link
        rel="preload"
        href="/fonts/DrukApp-Heavy.ttf"
        as="font"
        type="font/ttf"
        crossOrigin="anonymous"
      />
      <link
        rel="preload"
        href="/fonts/DrukApp-Heavy.woff"
        as="font"
        type="font/woff"
        crossOrigin="anonymous"
      />
    </>
  );
};

export default Fonts;
